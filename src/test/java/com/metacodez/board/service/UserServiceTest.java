package com.metacodez.board.service;

import com.metacodez.board.KaxiBoardApplication;
import com.metacodez.board.entity.User;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by Mante Luo on 4/28/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@SpringApplicationConfiguration(KaxiBoardApplication.class)
public class UserServiceTest {
    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Before
    public void setUp() throws Exception {
//        userService = new UserService();
    }

    @Test
    public void testCreateUser() throws Exception {
        User user = new User("helo", "g@c", "dkljfs");
        userService.createUser(user);
        assertThat(userService.getAll().size(), CoreMatchers.is(1));
    }

    @Test
    public void testAuthenticate() throws Exception {
        User user = new User("helo", "g@c", "dkljfs");
        userService.createUser(user);
        User user1 = userService.getAll().iterator().next();
        assertThat( passwordEncoder.matches("dkljfs",user1.getPassword()), is(true));
    }

    @Test
    public void testDeleteUser() throws Exception {
        User user = new User("helo", "g@c", "dkjfjdk");
        userService.createUser(user);
        assertThat(userService.getAll().size(), is(1));
        userService.deleteUser(user.getId());
        assertThat(userService.getAll().size(), is(0));
    }

    @Test
    public void testGetFollowed() throws Exception {
        User user1 = new User("helo", "g@c", "kjfd");
        userService.createUser(user1);
        User user2 = new User("helo1", "g@c2", "kjfd3df");
        userService.createUser(user2);
        user1.follow(user2);
        assertThat(userService.getFollowed(user2.getId()).size(), is(1));
        assertThat(userService.getFollowing(user1.getId()).size(), is(1));
    }

    @Test
    public void testGetAll() throws Exception {

    }

    @Test
    public void testFollow() throws Exception {

    }

    @Test
    public void testCreateReview() throws Exception {

    }
}