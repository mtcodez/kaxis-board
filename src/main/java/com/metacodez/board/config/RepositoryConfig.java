package com.metacodez.board.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by Mante Luo on 4/28/16.
 */
@Configuration
@EnableJpaRepositories(basePackages = "com.metacodez.board.repository")
public class RepositoryConfig {

}
