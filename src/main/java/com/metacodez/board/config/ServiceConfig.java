package com.metacodez.board.config;

import com.metacodez.board.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Mante Luo on 4/28/16.
 */
@Configuration
public class ServiceConfig {
    @Bean
    public UserService userService() {
        return new UserService();
    }
}
