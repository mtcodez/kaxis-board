package com.metacodez.board.config;

import com.metacodez.board.repository.UserRepository;
import com.metacodez.board.service.UserService;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by Mante Luo on 4/28/16.
 */
@Configuration
@EnableAutoConfiguration
@Import({RepositoryConfig.class, SecurityConfig.class, ServiceConfig.class})
public class TestConfig {

}
