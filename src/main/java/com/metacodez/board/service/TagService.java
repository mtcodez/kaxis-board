package com.metacodez.board.service;

import java.util.List;

import javax.transaction.Transactional;

import com.metacodez.board.entity.Review;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.stereotype.Service;

import com.metacodez.board.entity.Tag;
import com.metacodez.board.repository.TagRepository;

@Service
@Transactional
public class TagService {
	@Autowired
	private TagRepository repository;
	
	public List<Tag> getAll() {
		return repository.findAll();
	}
	public List<Review> getAllReviews(Tag tag) {
		List<Review> reviews =  repository.findOne(tag.getName()).getReviews();
        Hibernate.initialize(reviews);
        return reviews;
    }
}
