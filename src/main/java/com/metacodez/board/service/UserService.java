package com.metacodez.board.service;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.metacodez.board.entity.Review;
import com.metacodez.board.entity.User;
import com.metacodez.board.repository.ReviewRepository;
import com.metacodez.board.repository.UserRepository;

/**
 * Provide services for creating, updating and deleting users
 * @author Mante Luo
 *
 */
@Service
@Transactional
public class UserService {
	public UserService() {}
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ReviewRepository reviewRepository;
	@Autowired
    private PasswordEncoder passwordEncoder;

	public void createUser(User user) {
		// hash the password
        user.setPassword(passwordEncoder.encode(user.getPassword()));
		userRepository.save(user);
	}

    public void authenticate(User user) {

    }

	public boolean deleteUser(long id) {
		if (userRepository.exists(id)) {
			userRepository.delete(id);
			return true;
		} else {
			return false;
		}
	}

	public List<User> getFollowed(long id) {
		List<User> followed = (List<User>) userRepository.findOne(id).getFollowed();
		Hibernate.initialize(followed);
		return followed;
	}
    public List<User> getFollowing(long id) {
        List<User> following = userRepository.findOne(id).getFollowing();
        Hibernate.initialize(following);
        return following;
    }
	public List<User> getAll() {
		return userRepository.findAll();
	}
	public void follow(long id1, long id2) {
		User user1 = userRepository.findOne(id1);
		User user2 = userRepository.findOne(id2);
		user1.follow(user2);
	}
	public void createReview(long userId, Review review) {
		userRepository.findOne(userId).addReview(review);
	}
}
