package com.metacodez.board.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metacodez.board.entity.BookReview;
import com.metacodez.board.entity.MovieReview;
import com.metacodez.board.entity.MusicReview;
import com.metacodez.board.entity.Review;
import com.metacodez.board.entity.Tag;
import com.metacodez.board.repository.BookReviewRepository;
import com.metacodez.board.repository.MovieReviewRepository;
import com.metacodez.board.repository.MusicReviewRepository;
import com.metacodez.board.repository.ReviewRepository;

@Service
@Transactional
public class ReviewService {
	@Autowired
	ReviewRepository reviewRepository;
	@Autowired
	BookReviewRepository bookReviewRepository;
	@Autowired
	MovieReviewRepository movieReviewRepository;
	@Autowired
	MusicReviewRepository musicReviewRepository;
	
	public List<Review> getAll() {
		return reviewRepository.findAll();
	}
	
	public void addTag(long reivewId, Tag tag) {
		reviewRepository.findOne(reivewId).addTag(tag);
	}
	
	public void createBookReview(BookReview bookReview) {
		bookReviewRepository.save(bookReview);		
	}
	public void createMovieReview(MovieReview movieReview) {
		movieReviewRepository.save(movieReview);
	}
	public void createMusicReview(MusicReview musicReview) {
		musicReviewRepository.save(musicReview);
	}
	public void removeBookReview(long bookReviewId) {
		bookReviewRepository.delete(bookReviewId);		
	}
	public void removeMovieReview(long movieReviewId) {
		movieReviewRepository.delete(movieReviewId);
	}
	public void removeMusicReview(long musicReviewId) {
		musicReviewRepository.delete(musicReviewId);
	}
}
