package com.metacodez.board.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name="USERS")
public class User {
	public User() {}
	
	/**
	 * 
	 * @param username
	 * @param password store AS-IS, the hashing should be done in service
	 */
	public User(String username, String email, String password) {
		this.username = username;
		this.email = email;
		this.password = password;
		this.joinedDate = LocalDate.now();
        this.role = "USER";
	}

	public void follow(User user) {
		user.getFollowed().add(this);
		getFollowing().add(user);
	}
	
	public void addReview(Review review) {
		review.setUser(this);
		reviews.add(review);
	}

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", joinedDate=" + joinedDate +
                '}';
    }

    @Id
	@GeneratedValue
	@Column(name="USER_ID")
	private long id;
	
	@NotNull
    @Column(unique = true, nullable = false)
	private String username;
	@NotNull
    @Column(unique = true, nullable = false)
	private String email;

	@JsonIgnore
    @Column(length = 60, nullable = false)
	private String password;

	private LocalDate joinedDate;
    private String role;
	
	@ManyToMany(fetch=FetchType.LAZY
			, cascade=CascadeType.ALL)
	@JoinTable(joinColumns = @JoinColumn(name = "FOLLOWED_ID"),
		inverseJoinColumns = @JoinColumn(name = "FOLLOWING_ID"))
	private List<User> followed = new ArrayList<>();
	
	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinTable(joinColumns = @JoinColumn(name = "FOLLOWING_ID"),
		inverseJoinColumns = @JoinColumn(name = "FOLLOWED_ID"))
	private List<User> following = new ArrayList<>();
	
	@OneToMany(mappedBy="user", fetch=FetchType.LAZY,
			cascade=CascadeType.ALL)
	private List<Review> reviews = new ArrayList<>();
}
