package com.metacodez.board.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Movie {
    public Movie() {}
	@Id
	@GeneratedValue
	private long id;
	private String title;
	private String director;
	private LocalDate year;
}
