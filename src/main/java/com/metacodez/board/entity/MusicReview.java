package com.metacodez.board.entity;

import javax.persistence.Entity;

import lombok.Data;

@Data
@Entity
public class MusicReview extends Review {
	public MusicReview() {}
	public MusicReview(String title, String content) {
		super(title, content);
	}
}
