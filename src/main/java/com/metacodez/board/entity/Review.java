package com.metacodez.board.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class Review {
	public Review() {}
	
	public Review(String title, String content) {
		this.title = title;
		this.content = content;
		this.createdAt = LocalDate.now();
		this.updatedAt = LocalDate.now();
	}
	
	public void addTag(Tag tag) {
		tags.add(tag);
		tag.getReviews().add(this);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Column(name="REVIEW_ID")
	protected long id;
	
	protected LocalDate createdAt;
	protected LocalDate updatedAt;
	
	protected String title;
	protected String content;
	
	@ManyToOne
	@JoinColumn(name="USER_ID")
	protected User user;
	
	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinTable(joinColumns = @JoinColumn(name = "REVIEW_ID"),
			inverseJoinColumns = @JoinColumn(name = "TAG_ID"))
	protected List<Tag> tags = new ArrayList<>();
}
