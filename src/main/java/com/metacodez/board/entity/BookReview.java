package com.metacodez.board.entity;

import javax.persistence.Entity;

import lombok.Data;

@Data
@Entity
public class BookReview extends Review {
	public BookReview(){}
	public BookReview(String title, String content, String author) {
		super(title, content);
        this.author = author;
	}
    private String author;
    @Override
	public String toString() {
        return "BookReview(" + title + " by:" + author + ")";
    }
}
