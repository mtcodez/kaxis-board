package com.metacodez.board.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
public class Tag {
	public Tag() {}
	public Tag(String name) {
		this.name = name;
	}

	@Id
	@Column(nullable=false, unique=true)
	private String name;

	@ManyToMany(fetch=FetchType.LAZY
			, cascade=CascadeType.ALL, mappedBy = "tags")
	private List<Review> reviews = new ArrayList<>();
}
