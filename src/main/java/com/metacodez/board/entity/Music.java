package com.metacodez.board.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Music {
	public Music() {}
    @Id
    @GeneratedValue
    private long id;
	private String title;
	private String singer;
}
