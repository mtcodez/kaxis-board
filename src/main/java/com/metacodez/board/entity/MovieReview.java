package com.metacodez.board.entity;

import javax.persistence.Entity;

import lombok.Data;

@Entity
@Data
public class MovieReview extends Review {
	public MovieReview() {}
	public MovieReview(String title, String content) {
		super(title, content);	
	}
	
}
