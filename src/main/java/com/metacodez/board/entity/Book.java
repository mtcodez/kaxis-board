package com.metacodez.board.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Book {
    public Book() {}
	@Id
    @GeneratedValue
	private long id;
	private String title;
	private String author;
	private String publisher;
	private LocalDate year;
}
