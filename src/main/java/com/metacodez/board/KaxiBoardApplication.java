package com.metacodez.board;

import com.metacodez.board.entity.Tag;
import com.metacodez.board.service.ReviewService;
import com.metacodez.board.service.TagService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.metacodez.board.entity.BookReview;
import com.metacodez.board.entity.Review;
import com.metacodez.board.entity.User;
import com.metacodez.board.service.UserService;

@SpringBootApplication
public class KaxiBoardApplication {
	private static final Logger log = LoggerFactory.getLogger(KaxiBoardApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(KaxiBoardApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner demo(UserService userService, TagService tagService,
                                  ReviewService reviewService) {
		return (args) -> {
			User user1 = new User("mt", "mt@g", "dkjf");
			userService.createUser(user1);
			User user2 = new User("kx", "kx@g", "kjdf");
			userService.createUser(user2);
//			BookReview review1 = new BookReview("Howdy", "Good one there.", "Jon Casey");
//			Tag tag1 = new Tag("cool");
//            Tag tag2 = new Tag("cool");
//            review1.addTag(tag1);
//			reviewService.createBookReview(review1);
//
//			log.info("Number of users:" + userService.getAll().size());
//			userService.follow(user1.getId(), user2.getId());
//			userService.follow(user2.getId(), user1.getId());
//			log.info("--------------------------");
//			log.info("Following:" + userService.getFollowed(user1.getId()).size());
//			log.info("Following:" + userService.getFollowed(user2.getId()).size());
//            log.info("--------------------------");
//            log.info("Tag1:" + tagService.getAllReviews(tag1));
//            log.info("review1:" + review1.getTags());
//            log.info("tag2:" + tagService.getAllReviews(tag2));
		};
	}
}
