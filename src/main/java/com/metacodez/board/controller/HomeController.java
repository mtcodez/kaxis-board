package com.metacodez.board.controller;

import javax.ws.rs.core.Response;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
	@RequestMapping("/")
	public String greetings(Model model) {
        return "index";
	}

    @RequestMapping("/login")
    public String login(Model model) {
        return "login";
    }
}
