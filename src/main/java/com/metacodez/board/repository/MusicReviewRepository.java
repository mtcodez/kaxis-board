package com.metacodez.board.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.metacodez.board.entity.MusicReview;

@Repository
public interface MusicReviewRepository extends JpaRepository<MusicReview, Long>{

}
