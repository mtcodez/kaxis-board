package com.metacodez.board.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.metacodez.board.entity.Tag;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends JpaRepository<Tag, String>{
}
