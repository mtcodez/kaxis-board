package com.metacodez.board.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.metacodez.board.entity.MovieReview;

@Repository
public interface MovieReviewRepository extends JpaRepository<MovieReview, Long>{

}
