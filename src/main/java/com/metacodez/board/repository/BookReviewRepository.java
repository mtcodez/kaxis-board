package com.metacodez.board.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.metacodez.board.entity.BookReview;

@Repository
public interface BookReviewRepository extends JpaRepository<BookReview, Long> {

}
