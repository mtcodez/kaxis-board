package com.metacodez.board.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.metacodez.board.entity.Review;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long>{

}
